console.log('prodicts page');
 
import {
    products
} from "../js/data.js"
import {
    RENDER_PRODUCTS,
    tvs,
    phones,
    access,
    laptops
} from "../js/index.js"

window.onhashchange = () => {
    window.location.reload()
}

let id = +window.location.href.split('#')[1]

for (let item of products) {
    if (item.id === id) {
        let title = document.querySelector('.title')
        let imageS = document.querySelectorAll('.place_left img')
        let nameUser = document.querySelector('.seller p')
        let textUser = document.querySelector('.text__user')
        let price = document.querySelector('.cost_place p span')

        let counter = 0
        for (let img of imageS) {
            img.setAttribute('src', item.images[counter++])
            if (img.getAttribute('src') == "undefined") img.parentNode.style.display = 'none'
            img.onclick = () => {
                let main_href = mainImg.getAttribute('src')
                mainImg.setAttribute('src', img.getAttribute('src'))
                img.setAttribute('src', main_href)
            }
        }


        for (let name of item.comments) {
            nameUser.innerHTML = name.user
            textUser.innerHTML = name.text
        }

        title.innerHTML = item.name
        price.innerHTML = item.price + '$'

        if (item.category == 1) {
            RENDER_PRODUCTS(tvs, 'place-for-rec', 5)
        }
        if (item.category == 2) {
            RENDER_PRODUCTS(phones, 'place-for-rec', 5)
        }
        if (item.category == 3) {
            RENDER_PRODUCTS(access, 'place-for-rec', 5)
        }
        if (item.category == 4) {
            RENDER_PRODUCTS(laptops, 'place-for-rec', 5)
        }
    }
}