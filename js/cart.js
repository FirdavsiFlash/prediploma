let cart = localStorage.cart ? JSON.parse(localStorage.cart) : []
let fav = localStorage.fav ? JSON.parse(localStorage.fav) : []

 


import {products} from "./data.js"


localStorage.cart ? cart = JSON.parse(localStorage.cart) : console.log();
localStorage.fav ? fav = JSON.parse(localStorage.fav) : console.log();


const ADD_TO_CART = (id, btn, item) => {
    event.preventDefault()
    cart.push(id)
    localStorage.cart = JSON.stringify(cart)
    document.querySelector('.counter_card').innerHTML = `${JSON.parse(localStorage.cart).length}`
    
    let localArrTemp = JSON.parse(localStorage.arr)
    for(let product of localArrTemp){
        if(product.id === item.id){
            product.counter++
        }
    }
    localStorage.arr = JSON.stringify(localArrTemp)
    for (let good of cart) {
        if (good === id) {
            btn.innerHTML = 'Added'
            btn.classList.add('added-btn')
            console.log(true);     
        }
    }
}

const ADD_TO_FAV = (id, icon) => {

    // if()
    if (JSON.stringify(fav).includes(String(id))) {

        for (let num of fav) {
            if (num === id) {
                fav.splice(fav.indexOf(num), 1)
            }
        }
        localStorage.fav = JSON.stringify(fav)
        document.querySelector('.counter_favorite').innerHTML = `${JSON.parse(localStorage.fav).length}`
        icon.setAttribute('src', './img/like.svg')
    } else {
        event.preventDefault()
        fav.push(id)
        localStorage.fav = JSON.stringify(fav)
        document.querySelector('.counter_favorite').innerHTML = `${JSON.parse(localStorage.fav).length}`

        for (let good of fav) {
            if (good === id) {
                icon.setAttribute('src', './img/green.png')
            }
        }
    }

}


export {
    ADD_TO_CART,
    ADD_TO_FAV
}