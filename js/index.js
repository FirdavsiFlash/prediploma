import {
  products
} from "./data.js"
import {
  ADD_TO_CART,
  ADD_TO_FAV
} from "./cart.js"

const RENDER_PRODUCTS = (arr, id, limit) => {
  let place = document.querySelector(`#${id}`)

  let tempArr = arr.filter(() => true)
  tempArr.splice(0, limit)

  let btnShow = document.createElement('button')
  let btnHide = document.createElement('button')
  btnShow.innerHTML = 'Show all'
  btnHide.innerHTML = 'Hide all'
  btnShow.classList.add('btn__all', 'show__all')
  btnHide.classList.add('btn__all')
  btnHide.classList.add('hide__all')

  btnShow.onclick = () => {
    RENDER_PRODUCTS(arr, id, arr.length)
    btnHide.classList.add('hide__all')
    place.append(btnHide)
  }
  btnHide.onclick = () => {
    btnHide.style.display = 'none'
    RENDER_PRODUCTS(arr, id, 5)
  }

  place.innerHTML = ''
  place.append(btnShow)
  for (let item of arr) {
    let a = document.createElement('a')
    let div = document.createElement('div')
    let likeIcon = document.createElement('img')
    let img = document.createElement('img')
    let category = document.createElement('p')
    let title = document.createElement('p')
    let btnColors = document.createElement('div')
    let grey = document.createElement('button')
    let light = document.createElement('button')
    let dark = document.createElement('button')
    let gold = document.createElement('button')
    let product_desc = document.createElement('div')
    let product_prise = document.createElement('p')
    let like_comm = document.createElement('div')
    let likes = document.createElement('p')
    let like = document.createElement('span')
    let imgLike = document.createElement('img')
    let comments = document.createElement('p')
    let comment = document.createElement('span')
    let imgComment = document.createElement('img')
    let button = document.createElement('button')
    let btnForLike = document.createElement('button')

    // ADD_TO_FAV(item.id, likeIcon, true)
    likeIcon.setAttribute('src', './img/like.svg')
    favIcon(item.id, likeIcon)
    category.innerHTML = item.category
    title.innerHTML = item.name
    product_prise.innerHTML = `${item.price} <br> $`
    like.innerHTML = '4'
    comment.innerHTML = '25'
    button.innerHTML = 'Add'

    button.onclick = () => {
      event.preventDefault()
      ADD_TO_CART(item.id, button, item)
    }


    div.classList.add('active')
    div.classList.add('product')
    likeIcon.classList.add('like_green')
    img.classList.add('product_img')
    category.classList.add('categories')
    title.classList.add('product_name')
    btnColors.classList.add('btns')
    grey.classList.add('color', 'grey')
    light.classList.add('color', 'light')
    dark.classList.add('color', 'dark')
    gold.classList.add('color', 'gold')
    product_desc.classList.add('product_desc')
    product_prise.classList.add('product_prise')
    like_comm.classList.add('like_comm')
    likes.classList.add('likes', 'l')
    like.classList.add('like')
    comments.classList.add('comments', 'l')
    comment.classList.add('comment')
    button.classList.add('buy')
    likeIcon.classList.add('green-shit')

    
    img.setAttribute('src', `${item.images[0]}`)

    imgLike.setAttribute('src', './images/icons/btn-star.svg')
    imgComment.setAttribute('src', './images/icons/btn-commend.svg')

    a.setAttribute('href', './product-info.html#' + item.id)

    likeIcon.onclick = () => {
      event.preventDefault()
      ADD_TO_FAV(item.id, likeIcon)
    }

    likes.append(like, imgLike)
    comments.append(imgComment, comment)
    like_comm.append(likes, comments)
    product_desc.append(product_prise, like_comm)
    btnColors.append(grey, light, dark, gold)
    btnForLike.append(likeIcon)
    div.append(btnForLike, img, category, title, btnColors, product_desc, button)
    place.append(a)
    a.append(div)

    if (arr.indexOf(item) >= limit) {
      div.style.display = 'none'
      div.classList.remove('active')
    }

    addBtn(item.id, button)

  }
}

let sorted = products.sort((a, b) => b.price - a.price);
let cheap = []
let tvs = []
let phones = []
let access = []
let laptops = []

for (let item of products) {
  item.category == 1 ? tvs.push(item) : console.log();
  item.category == 2 ? phones.push(item) : console.log();
  item.category == 3 ? access.push(item) : console.log();
  item.category == 4 ? laptops.push(item) : console.log();
  if (item.price < 2000) {
    cheap.push(item)
    cheap.sort((a, b) => b.price - a.price)
    cheap.reverse()
  }
}


function addBtn(id, btn) {
  if (localStorage.cart) {
    for (let good of JSON.parse(localStorage.cart)) {
      if (good === id) {
        btn.innerHTML = 'Added'
        btn.classList.add('added-btn')
      }
    }
  }
}

function favIcon(id, icon) {
  if (localStorage.fav) {
    for (let ic of JSON.parse(localStorage.fav)) {
      if (ic === id) {
        if (icon.getAttribute('src') == './img/like.svg') {
          icon.setAttribute('src', './img/green.png')
        } else {
          icon.setAttribute('src', './img/like.svg')
        }
      }
    }
  }
}


if (document.body.getAttribute('id') == 'home') {
  RENDER_PRODUCTS(sorted, 'place-for-expensive', 5)
  RENDER_PRODUCTS(cheap, 'place-for-cheaper', 5)
  RENDER_PRODUCTS(phones, 'place-for-phones', 5)
  RENDER_PRODUCTS(access, 'place-for-access', 5)
}
if (document.body.getAttribute('id') == 'catalog-products') {
  RENDER_PRODUCTS(tvs, 'place-for-tv', 5)
  RENDER_PRODUCTS(phones, 'place-for-phones', 5)
  RENDER_PRODUCTS(laptops, 'place-for-laptop', 5)
  RENDER_PRODUCTS(access, 'place-for-accessory', 5)
}

if (document.body.getAttribute('id') == 'pagination') {
  RENDER_PRODUCTS(products, 'place-for-all', products.length)
}

if (document.body.getAttribute('id') == 'product-info') {
  for (let item of products) {
    if (item.category == 1) {
      RENDER_PRODUCTS(tvs, 'place-for-rec', 5)
    }
    if (item.category == 2) {
      RENDER_PRODUCTS(phones, 'place-for-rec', 5)
    }
    if (item.category == 3) {
      RENDER_PRODUCTS(access, 'place-for-rec', 5)
    }
    if (item.category == 4) {
      RENDER_PRODUCTS(laptops, 'place-for-rec', 5)
    }
  }
}

export {
  RENDER_PRODUCTS,
  tvs,
  phones,
  access,
  laptops
}