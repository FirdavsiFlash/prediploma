import {
    products
} from "./data.js"

if (localStorage.cart) {
    let cartId = JSON.parse(localStorage.cart) || []
    let cartObj = []
    let table = document.querySelector("table tbody")
    let localArr = JSON.parse(localStorage.arr)

    const MANAGE_COUNT = ({
        plus,
        id
    }) => {
        let cart = JSON.parse(localStorage.cart)

        if (plus == true) {
            cart.push(id)
        } else if (plus == false) {
            cart.splice(cart.indexOf(id), 1)
        } else {
            cart = cart.filter(item => item != id)
            
            localArr[id].counter = 0
            localStorage.arr = JSON.stringify(localArr)

        }

        localStorage.cart = JSON.stringify(cart)
        if (id) {
            if (plus == true) {
                CYCLE(id, true)
            }
            if (plus == false) {
                CYCLE(id, false)
            }
        }
        CREATE_CART()


    }
    const CYCLE = (idx, bool) => {
        let localArr = JSON.parse(localStorage.arr)
        if (idx) {
            for (let item of products) {
                for (let id of cartId) {
                    if (item.id == idx) {
                        if (!cartObj.includes(item)) {
                            cartObj.push(item)
                        } else {
                            localArr.forEach(element => {
                                if (element.id == id) {
                                    if (bool == true) {
                                        element.counter++
                                    }
                                    if (bool == false) {
                                        element.counter--
                                    }
                                }
                            });
                            if (bool == true) {
                                item.counter++
                            }
                            if (bool == false) {
                                item.counter--
                            }
                        }
                        return
                    }
                }
            }
        } else {
            for (let item of products) {
                for (let id of cartId) {
                    if (item.id == id) {
                        if (!cartObj.includes(item)) {

                            cartObj.push(item)
                        } else {
                            localArr.forEach(element => {
                                if (element.id == id) {
                                    element.counter++
                                }
                            });
                            item.counter++
                        }
                    }
                }
            }
        }
        localStorage.arr = JSON.stringify(localArr)
        CREATE_CART()
    }
    const CREATE_CART = () => {
        table.innerHTML = ""
        for (let item of cartObj) {

            let tr = document.createElement("tr")
            let td_idx = document.createElement("td")
            let td_name = document.createElement("td")
            let td_count = document.createElement("td")
            let td_sale = document.createElement("td")
            let td_dis = document.createElement('td')
            let end_price = document.createElement('td')
            let num = document.createElement('td')
            let move = document.createElement('div')
            let plus = document.createElement('button')
            let coun = document.createElement('p')
            let minus = document.createElement('button')
            let delete_all = document.createElement("button")

            delete_all.innerHTML = "delete all"
            delete_all.classList.add('delete-all')

            let counter = document.createElement('span')
            let count_plus = document.createElement('span')
            let count_minus = document.createElement('span')

            counter.innerText = +(JSON.parse(localStorage.cart).filter(id => id === item.id)).length
            count_plus.classList.add('manageCart')
            count_minus.classList.add('manageCart')

            count_plus.innerText = "+"
            count_minus.innerText = "-"

            table.classList.add('tab')
            plus.classList.add('plus')
            minus.classList.add('minus')
            move.classList.add('move')
            coun.classList.add('coun')

            coun.innerHTML = +(JSON.parse(localStorage.cart).filter(id => id === item.id)).length
            td_idx.innerText = +cartObj.indexOf(item) + 1
            td_name.innerText = item.name

            td_count.append(count_minus, counter, count_plus)

            td_sale.innerText = `${item.price} $`
            td_dis.innerText = `${(item.counter + 1) * item.price} $`

            minus.innerHTML = '-'
            plus.innerHTML = '+'

            num.append(delete_all)

            if (item.id === JSON.parse(localStorage.cart)) num.innerHTML++
            move.append(plus, coun, minus)
            tr.append(td_idx, td_name, td_count, td_sale, td_dis, end_price, num)
            table.append(tr)

            count_minus.onclick = () => MANAGE_COUNT({
                id: item.id,
                plus: false
            })
            count_plus.onclick = () => MANAGE_COUNT({
                id: item.id,
                plus: true
            })
            delete_all.onclick = () => MANAGE_COUNT({
                id: item.id
            })
        }
    }
    CYCLE()

    CREATE_CART()
}