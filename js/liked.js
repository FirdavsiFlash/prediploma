if (localStorage.fav) {
    let table = document.querySelector("table tbody")

    let localArr = JSON.parse(localStorage.arr)

    let favObj = []
    let favId = JSON.parse(localStorage.fav)

    const manageLike = (id) => {
        for (let item of favId) {
            if (item === id) {
                console.log(item);
                favId = favId.filter(item => item != id)
                localStorage.fav = JSON.stringify(favId)
                sortArr()
            }
        }
        window.location.reload()
    }

    const sortArr = () => {
        for (let item of localArr) {
            for (let id of favId) {
                if (item.id == id) {
                    if (!favObj.includes(item)) {
                        favObj.push(item)
                    }
                }
            }
        }
        renderLikesList()
        
    }
    const renderLikesList = () => {
        console.log('render');
        table.innerHTML = ""
        for (let item of favObj.sort()) {
            let tr = document.createElement('tr')
            let a = document.createElement('a')
            let td_idx = document.createElement('td')
            let td_name = document.createElement('td')
            let icon = document.createElement('img')
            icon.src = '../img/liked.png'
            icon.classList.add('icon-like')

            icon.onclick = () => {
                manageLike(item.id)
            }

            td_idx.innerHTML = `${+favObj.indexOf(item) + 1}.`
            a.innerHTML = `${item.name}`
            a.setAttribute('href', './product-info.html#' + item.id)
            td_name.append(a)

            tr.append(td_idx, td_name, icon)
            table.append(tr)
        }
    }
    sortArr()



}