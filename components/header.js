let header = document.querySelector('header')
header.innerHTML = `
    <div class="header__top">
        <div class="header-line-top"></div>
            <div class="header__top_left">
                <a href="./index.html">
                    <img src="./images/Logo.svg" alt="">
                </a>
                <a href="#">
                    <p class="header-nav">Доска объявлений</p>
                </a>

                <a href="#">
                    <p class="header-nav">Сервисный центр</p>
                </a>
                <a href="#">
                    <p class="header-nav">Интернет-магазин Dily.ru</p>
                </a>
                <a href="#">
                    <p class="header-nav">Скупка</p>
                </a>
                
            </div>
            <div class="header__top_right">
                <div class="header-r-nav">
                    <img src="./images/icons/location.svg" alt="">
                    <select class="select" name="" id="">
                        <option value="">Москва</option>
                        <option value="">Москва</option>
                        <option value="">Москва</option>
                        <option value="">Москва</option>
                    </select>
                </div>
                <div class="header-r-nav">
                    <img src="./images/icons/person.svg" alt="">
                    <p class="header-r-reg">
                        <p class="shit">Вход </p> / <p> регистрация</p>
                    </p>
                </div>
            </div>
        </div>
    
    <div class="bottom item">
        <div class="b-main">
            <div class="m-left">
                <div class="key">
                    <img src="./images/icons/menu-product.svg" class="menu">
                    <a href="./products.html" class="active">Каталог товаров</a>
                </div>
                <div class="key">
                    <img src="./images/icons/dostavka.svg" alt="">
                    <span>Доставка и оплата</span>
                </div>
            </div>
            <div class="m-right">
                <div class="search">
                    <img src="./images/icons/search.svg">
                    <input class="searh-request" type="search" placeholder="Я хочу купить">
                    <button>Найти</button>

                    <div class="match"></div>
                </div>
                <div class="other">
                    <div class="key">
                        <p><a href="#">Поддержка</a></p>
                    </div>
                    <div class="key">
                        <img src="./images/icons/btn list.svg" alt=""><img>
                    </div>
                    <div class="key">   
                        <a href="./liked.html">
                            <img src="./images/icons/btn-like.svg" alt="">
                            <div class="counter_favorite tablo"></div>
                        <a>
                    </div>
                    <div class="key">
                        <a href="./cart.html">
                            <img src="./images/icons/icon cart.svg" alt="">
                            <div class="counter_card tablo"> </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
`
import { products } from "../js/data.js"
if(localStorage.cart){
    document.querySelector('.counter_card').innerHTML = `${JSON.parse(localStorage.cart).length}`
}
if(localStorage.fav){
    document.querySelector('.counter_favorite').innerHTML = `${JSON.parse(localStorage.fav).length}`
}
 

let search = document.querySelector('header input')
let match = document.querySelector('header .match')
search.onkeyup = (e) => {
    if (event.keyCode !== 38 && event.keyCode !== 40 && event.keyCode !== 13) {
        if (e.target.value.trim().length >= 2) {
            let result = products.filter(item => item.name.trim().toLocaleLowerCase().includes(e.target.value.trim().toLowerCase()))

            if (products.length) {
                match.innerHTML = ''
                for (let item of result) {
                    let a = document.createElement('a')
                    a.classList.add('link-search')
                    a.innerHTML = item.name
                    a.href = `./product-info.html#${item.id}`
                    match.append(a)
                    match.classList.add('active')
                }
                return
            }
        } else match.classList.remove('active')
    }
}
let counter = 0
document.onkeydown = () => {
    let children = document.querySelectorAll('.match a')

    if (event.keyCode == 38) {
        // UP
        counter--

        if(counter < 0) {
            counter = children.length - 1
        }
        children[counter].classList.add('active')

        // Remove first element active class
        if (counter > 0) {
            children[0].classList.remove("active")
        }

        if(children[counter + 1]) children[counter + 1].classList.remove('active')
    } else if (event.keyCode == 40) {
        // DOWN
        // Удаляем класс у предыдущего ребенка
        if (children[counter - 1]) children[counter - 1].classList.remove("active")
        // Даем класс актуальному элементу
        children[counter].classList.add("active")

        // Remove last element active class
        if (counter == 0) {
            children[children.length - 1].classList.remove("active")
        }
        // Переходим к первому, если дальше некуда  
        if (counter + 1 == children.length) counter = 0
        else counter++

    } else if (event.keyCode == 13) {
        window.location.href = document.querySelector('.match a.active').getAttribute('href')
    }
}